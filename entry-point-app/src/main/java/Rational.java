import java.util.Comparator;

import static java.lang.Math.abs;

public class Rational implements Comparable<Rational> {

    private int mNominator;
    private int mDenominator;

    Rational(Rational rational) {
        this.mNominator = rational.mNominator;
        this.mDenominator = rational.mDenominator;
    }

    Rational(int nominator, int denominator) {
        if (denominator <= 0) {
            System.out.println("Знаменатель не может быть равен нулю или отрицательному числу!");
            System.exit(0);
        }
        int nod = gcd(abs(nominator), abs(denominator));
        this.mNominator = nominator / nod;
        this.mDenominator = denominator / nod;
    }

    Rational(int nominator) {
        this.mNominator = nominator;
        this.mDenominator = 1;
    }

    Rational() {
        this.mNominator = 0;
        this.mDenominator = 1;
    }

    Rational(String str) {
        int i = 0;
        String tmp = "";
        if (str.charAt(i) == '-') {
            tmp += str.charAt(i);
            i++;
        }
        if (!Character.isDigit(str.charAt(i))) {
            System.out.println("Некорректный ввод. Обнаружен посторонний символ.");
            System.exit(0);
        }
        while ((i < str.length()) && Character.isDigit(str.charAt(i))) {
            tmp += str.charAt(i);
            i++;
        }
        this.mNominator = Integer.parseInt(tmp);
        tmp = "";
        if (i < (str.length())) {
            if (str.charAt(i) == '/') {
                i++;
                if (i == (str.length()) || !Character.isDigit(str.charAt(i))) {
                    System.out.println("Некорректный ввод.");
                    System.exit(0);
                }
                while ((i < str.length()) && Character.isDigit(str.charAt(i))) {
                    tmp += str.charAt(i);
                    i++;
                }
                if (i < str.length()) {
                    System.out.println("Некорректный ввод. Обнаружен посторонний символ.");
                    System.exit(0);
                }
                if (tmp.equals("0")) {
                    System.out.println("Знаменатель не может быть равен нулю.");
                    System.exit(0);
                }
                this.mDenominator = Integer.parseInt(tmp);
            } else {
                System.out.println("Некорректный ввод.");
                System.exit(0);
            }
        } else {
            this.mDenominator = 1;
        }
        int nod = gcd(abs(this.mNominator), abs(this.mDenominator));
        this.mNominator = this.mNominator / nod;
        this.mDenominator = this.mDenominator / nod;
    }

    public int getNominator() {
        return mNominator;
    }

    public int getDenominator() {
        return mDenominator;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Rational rat = (Rational) obj;
        return this.mNominator == rat.mNominator;
    }

    @Override
    public int hashCode() {
        return this.mNominator * 888 + 365;
    }

    @Override
    public int compareTo(Rational rational) {
        return (this.mNominator - rational.mNominator);
    }

    public static Comparator<Rational> NominatorCompare = new Comparator<Rational>() {

        @Override
        public int compare(Rational r1, Rational r2) {
            return r1.mNominator - r2.mNominator;
        }
    };


    void add(Rational r) {
        int c = (this.mDenominator * r.mDenominator) / gcd(abs(mDenominator), abs(r.mDenominator));
        this.mNominator = this.mNominator * (c / this.mDenominator);
        this.mNominator = this.mNominator + r.mNominator * (c / r.mDenominator);
        int f = gcd(abs(this.mNominator), abs(c));
        this.mNominator = this.mNominator / f;
        this.mDenominator = c / f;
    }

    protected void sub(Rational r) {
        int c = (this.mDenominator * r.mDenominator) / gcd(abs(mDenominator), abs(r.mDenominator));
        this.mNominator = this.mNominator * (c / this.mDenominator);
        this.mNominator = this.mNominator - r.mNominator * (c / r.mDenominator);
        int f = gcd(abs(this.mNominator), abs(c));
        this.mNominator = this.mNominator / f;
        this.mDenominator = c / f;
    }

    void mul(Rational r) {
        this.mNominator = this.mNominator * r.mNominator;
        this.mDenominator = this.mDenominator * r.mDenominator;
        int c = gcd(abs(this.mNominator), abs(this.mDenominator));
        this.mNominator = this.mNominator / c;
        this.mDenominator = this.mDenominator / c;
    }

    void div(Rational r) {
        int c;
        int b;
        if (r.mNominator < 0) {
            c = -r.mDenominator;
            b = -r.mNominator;
        } else {
            c = r.mDenominator;
            b = r.mNominator;
        }
        this.mNominator = this.mNominator * c;
        this.mDenominator = this.mDenominator * b;
        c = gcd(abs(this.mNominator), abs(this.mDenominator));
        this.mNominator = this.mNominator / c;
        this.mDenominator = this.mDenominator / c;
    }

    void power(int p) {
        if (p < 0) {
            int a = this.mDenominator;
            this.mDenominator = this.mNominator;
            this.mNominator = a;
        }
        this.mNominator = (int) Math.pow(this.mNominator, abs(p));
        this.mDenominator = (int) Math.pow(this.mDenominator, abs(p));
        if (this.mDenominator < 0) {
            this.mDenominator = -this.mDenominator;
            this.mNominator = -this.mNominator;
        }
    }

    double toDouble() {
        return ((double) this.mNominator / this.mDenominator);
    }

    @Override
    public String toString() {
        if (this.mDenominator != 1 && this.mNominator != 0) {
            return (String.valueOf(this.mNominator) + "/" + String.valueOf(this.mDenominator));
        } else {
            return String.valueOf(this.mNominator);
        }
    }

    int gcd(int a, int b) {
        int c;
        if (a > b) {
            a = a + b;
            b = a - b;
            a = a - b;
        }
        if (a != 0) {
            while ((b % a) != 0) {
                c = b % a;
                b = a;
                a = c;
            }
            return a;
        } else {
            return 1;
        }
    }

    boolean isLess(Rational rat) {
        if (this.mNominator * rat.mDenominator > this.mDenominator * rat.mNominator) {
            return false;
        } else {
            return true;
        }
    }

    boolean isEqual(Rational rat) {
        if ((this.mNominator == rat.mNominator) && (this.mDenominator == rat.mDenominator)) {
            return true;
        } else {
            return false;
        }
    }

}
